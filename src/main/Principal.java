package main;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

import datos.Administrador;
import datos.CasaRural;
import datos.Hotel;
import datos.Reserva;
import datos.ReservaCompleta;
import datos.Usuario;
import datos.Visitante;

public class Principal {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		//declaración de variables
		byte opcionMenu;
		String nombreUsuario, passwordUsuario;
	
//creación usuario administrador
		Usuario.usuariosHashMap.put("admin",new Administrador("admin", "1234"));

//volcado de alojamientos desde el archivo alojamientos.csv en el ArrayList
		Administrador.volcadoAlojamientos();
		
//volcado de usuarios desde archivo usuarios.csv en el HashMap
		Usuario.volcadoUsuarios();

//MENÚ BIENVENIDA		
		do {
			System.out.println("Bienvenido a la aplicación de reservas de "
					+ "alojamientos para vacaciones");
			System.out.println();
			System.out.println("Elija una opción:");
			System.out.println("1. Registrarse.");
			System.out.println("2. Identificarse. ");
			System.out.println("3. Salir del programa");
			
			while(!sc.hasNextByte()) {
				System.out.println("Introduzca un número entero del 1 al 3");
				sc.nextLine();
			}
			opcionMenu= sc.nextByte();
			sc.nextLine();
			System.out.println();		
			
			switch (opcionMenu) {
//registro de nuevos usuarios
			case 1:
			{
				do {
					do {	
						System.out.println("Introduzca el nombre de usuario:");
						nombreUsuario=sc.nextLine();
						if(Usuario.usuariosHashMap.containsKey(nombreUsuario))
						{
							System.out.println("El nombre de usuario "
									+ "introducido ya está siendo utilizado,"
									+ " introduzca otro nombre de usuario");
						}
					}while (Usuario.usuariosHashMap.containsKey(nombreUsuario));	
					System.out.println("Introduzca la contraseña:");
					passwordUsuario= sc.nextLine();
					if(nombreUsuario.isEmpty() || passwordUsuario.isEmpty()) 
						{
							System.out.println("El nombre de usuario o la "
									+ "contraseña no pueden estar vacíos");
						}
					}while(nombreUsuario.isEmpty()||passwordUsuario.isEmpty());
					//si el nombre no existe previamente, el nombre y la 
					//constraseña no están vacías, se añade el nuevo usuario
				
				Usuario.usuariosHashMap.put(nombreUsuario, 
							new Visitante(nombreUsuario, passwordUsuario));
				escribirUsuarios();
				System.out.println("Usuario registrado con éxito");
				System.out.println();
			}break;
//identificación de usuarios			
			case 2: 
			{
				do {
					do {
						System.out.println("Introduzca el nombre de usuario:");
						nombreUsuario=sc.nextLine();
						
						System.out.println("Introduzca la contraseña:");
						passwordUsuario= sc.nextLine();
						 if(nombreUsuario.isEmpty()||passwordUsuario.isEmpty()) 
							{
								System.out.println("El nombre de usuario y la "
										+ "contraseña no pueden estar vacíos");
							}
					}while(nombreUsuario.isEmpty()||passwordUsuario.isEmpty());
					
					if (!Usuario.usuariosHashMap.containsKey(nombreUsuario))
					{
						System.out.println("Nombre de usuario no encontrado");					
					}
					else if ((Usuario.usuariosHashMap.
							containsKey(nombreUsuario)) &&
							(!Usuario.usuariosHashMap.get(nombreUsuario).
									getPassword().equals(passwordUsuario)))
					{
				System.out.println("Contraseña no válida");
					}
					else if (Usuario.usuariosHashMap.get(nombreUsuario).
							getPassword().equals(passwordUsuario)) 
					{
						if(Usuario.usuariosHashMap.get(nombreUsuario) 
								instanceof Administrador)
							menuAdministrador();
						if(Usuario.usuariosHashMap.get(nombreUsuario) 
								instanceof Visitante)
							menuVisitante(nombreUsuario);
					}
				}while(nombreUsuario.equals(null)||passwordUsuario.equals(null)
						||!Usuario.usuariosHashMap.containsKey(nombreUsuario)
						||!Usuario.usuariosHashMap.get(nombreUsuario).
							getPassword().equals(passwordUsuario));
				System.out.println();
				
			}break;
//salir del programa
			case 3: 
			{
				salir();break;
			}
			default:  break;
			}
		}while(opcionMenu!=3);

		sc.close();
	}
	
//método para lanzar el menú Administrador	
	public static void menuAdministrador() 
	{
		Scanner sc = new Scanner(System.in);
		//variables propias del método
		Byte opcionMenu, tipoAlojamiento;
		String nombre, tienePiscina;
		double precioNoche; 
		byte numPersonas, numHabitaciones;
		int indice, numEstrellas;
		boolean piscina = false;
		
		do {
			System.out.println();
			System.out.println("============================================");
			System.out.println("============== ADMINISTRADOR ===============");
			System.out.println("============================================");
			System.out.println();
			System.out.println("Elija una opción:");
			System.out.println("1. Dar de alta nuevo alojamiento.");
			System.out.println("2. Listar alojamientos.");
			System.out.println("3. Eliminar alojamiento.");
			System.out.println("4. Salir.");
		
			while(!sc.hasNextByte()) 
			{
				System.out.println("Introduzca un número entero del 1 al 4");
				sc.nextLine();
			}
		opcionMenu= sc.nextByte();
		sc.nextLine();
		System.out.println();
		
		switch (opcionMenu) {
		case 1: 
			{//dar de alta un nuevo alojamiento
				do {
				System.out.println("Elija tipo de alojamiento a añadir:");
				System.out.println();
				System.out.println("1. Para añadir una Casa Rural.");
				System.out.println("2. Para añadir un Hotel");
				while(!sc.hasNextByte()) {
					System.out.println("Introduzca 1 para añadir una Casa Rural"
							+ " o 2 para añadir un Hotel");
					sc.nextLine();
				}				
				tipoAlojamiento = sc.nextByte();
				sc.nextLine();
				}while(tipoAlojamiento!=1 && tipoAlojamiento!=2);
				
				if (tipoAlojamiento==1)
				{
					System.out.println("=====================");
					System.out.println("INTRODUCIR CASA RURAL");
					System.out.println("=====================");
					System.out.println("Indique el nombre del alojamiento:");
					nombre=sc.nextLine();
					System.out.println("Indique el precio por noche (00,00):");
					while(!sc.hasNextDouble()) {
						System.out.println("Indique el precio usando la coma "
								+ "(,) para introducir los decimales:");
					}
					precioNoche = sc.nextDouble();
					sc.nextLine();
					System.out.println("Indique el número de personas:");
					while(!sc.hasNextByte()) {
						System.out.println("Indique un número entero");
					}
					numPersonas= sc.nextByte();
					sc.nextLine();
					System.out.println("Indique el número de habitaciones:");
					while(!sc.hasNextByte()) {
						System.out.println("Indique un número entero");
					}
					numHabitaciones = sc.nextByte();
					sc.nextLine();
					Administrador.darAltaAlojamiento(new CasaRural(nombre, 
							precioNoche, numPersonas, numHabitaciones));
				}	
				else if (tipoAlojamiento==2)
				{
					System.out.println("================");
					System.out.println("INTRODUCIR HOTEL");
					System.out.println("================");
					System.out.println("Indique el nombre del alojamiento:");
					nombre=sc.nextLine();
					System.out.println("Indique el precio por noche (00,00):");
					while(!sc.hasNextDouble()) {
						System.out.println("Indique el precio usando la coma "
								+ "(,) para introducir los decimales:");
					}
					precioNoche = sc.nextDouble();
					sc.nextLine();
					System.out.println("Indique el número de estrellas (1-5):");
					while(!sc.hasNextInt()) {
						System.out.println("Indique un número entero entre el "
								+ "1 y el 5 (ambos inclusive");
					}
					numEstrellas = sc.nextInt();
					sc.nextLine();
					do {
					System.out.println("Indique si el hotel tiene piscina "
							+ "(s/n):");
					tienePiscina = sc.nextLine();
					}while(!tienePiscina.equalsIgnoreCase("s")&&
							(!tienePiscina.equalsIgnoreCase("n")));
					if (tienePiscina.equalsIgnoreCase("s"))
						piscina=true;
					else if (tienePiscina.equalsIgnoreCase("n"))
						piscina=false;
					Administrador.darAltaAlojamiento(new Hotel
							(nombre, precioNoche,	numEstrellas, piscina));
				}	
			}break;
		case 2:
			{//listar alojamientos
				System.out.println("====================");
				System.out.println("LISTADO ALOJAMIENTOS");
				System.out.println("====================");
				listarAlojamientos();
			}break;
		case 3:
			{//eliminar alojamiento
				System.out.println("====================");
				System.out.println("ELIMINAR ALOJAMIENTO");
				System.out.println("====================");
				System.out.println("Introduzca el número de alojamiento que "
						+ "desea eliminar");
				while(!sc.hasNextInt()) 
				{
						System.out.println("Introduzca un número entero.");
						sc.nextLine();
				}
				indice=sc.nextInt();
				sc.nextLine();
				Administrador.darBajaAlojamiento(Administrador.alojamientos.
						get(indice-1));
				
			}break;
		case 4: 
			{//salir del programa
				escribirAlojamientos(); //se escribe el archivo de alojamientos
				salir(); //se sale de la aplicación
			}break;
		default:  break;
		}
	}while(opcionMenu!=4);
	sc.close();	
	}

//método para lanzar el menú Visitante	
	public static void menuVisitante(String nombreUsuario) 
	{
		Scanner sc =new Scanner(System.in);
		//variables propias del método
		byte opcionMenu;
		String tipo;
		double precio;
		int numePersonas, numEstrellas, numDias, numAlojamiento;
		String fechaEntrada = null;
		
		do {
			System.out.println();
			System.out.println("============================================");
			System.out.println("================ VISITANTE =================");
			System.out.println("============================================");
			System.out.println();
			System.out.println("Elija una opción:");
			System.out.println("1. Buscar alojamientos por tipo.");
			System.out.println("2. Buscar alojamientos por precio máximo.");
			System.out.println("3. Buscar casas rurales por capacidad.");
			System.out.println("4. Buscar hoteles por categoría.");
			System.out.println("5. Ver reservas.");
			System.out.println("6. Reservar.");
			System.out.println("7. Salir del programa");
			
			while(!sc.hasNextByte()) {
				System.out.println("Introduzca un número entero del 1 al 7");
				sc.nextLine();
			}
			
			opcionMenu= sc.nextByte();
			sc.nextLine();
			System.out.println();
			
			switch (opcionMenu) {
			case 1: 
			{
				System.out.println("================================");
				System.out.println("Búsqueda por tipo de alojamiento");
				System.out.println("================================");
				do {	
					System.out.println("Indique el tipo de alojamiento que "
							+ "desea buscar:");
					System.out.println("\"Casa Rural\" o \"Hotel\"");
					System.out.println();
										
					tipo=sc.nextLine();
				}while(!tipo.equalsIgnoreCase("casa rural") && 
						!tipo.equalsIgnoreCase("hotel"));
				busquedaAlojamientoPorTipo(tipo);
			}break;
			case 2: 
			{
				System.out.println("==========================");
				System.out.println("Búsqueda por precio máximo");
				System.out.println("==========================");
				System.out.println("Indique el precio máximo por el que desea "
						+ "filtrar:");
				while(!sc.hasNextDouble()) {
					System.out.println("Indique el precio usando la coma "
							+ "(,) para introducir los decimales:");
					sc.nextLine();
				}
				precio = sc.nextDouble();
				sc.nextLine();
				
				busquedaAlojamientoPorPrecioMax(precio);
			}break;
			case 3: 
			{
				System.out.println("====================================");
				System.out.println("Búsqueda de Casa Rural por capacidad");
				System.out.println("====================================");
				System.out.println("Indique el número de personas por el "
						+ "que desea filtrar:");
				while(!sc.hasNextInt()) {
					System.out.println("Indique el número de personas:");
					sc.nextLine();
				}
				numePersonas = sc.nextInt();
				sc.nextLine();
				
				busquedaCasaRuralPorNumPersonas(numePersonas);
			}break;
			case 4:
			{
				System.out.println("===============================");
				System.out.println("Búsqueda de Hotel por categoría");
				System.out.println("===============================");
				System.out.println("Indique el número de estrellas por el que "
						+ "desea filtrar:");
				while(!sc.hasNextInt()) {
					System.out.println("Indique el número de estrellas:");
					sc.nextLine();
				}
				numEstrellas = sc.nextInt();
				sc.nextLine();
				
				busquedaHotelPorEstrellas(numEstrellas);
			}break;
			case 5://Ver reservas
			{
//volcado de las reservas desde el archivo reservas.csv en ArrayList.		
				ReservaCompleta.volcadoArchivoReservas();
				ReservaCompleta.mostrarReservas(nombreUsuario);	
			}
				break;
			case 6://Reservar
			{
				System.out.println("===============================");
				System.out.println("Realizar reserva de alojamiento");
				System.out.println("===============================");
				listarAlojamientos();
					
				System.out.println("Indique el número de alojamiento que "
						+ "desea reservar");
				while(!sc.hasNextInt()) {
					System.out.println("Introduzca el número de alojamiento");
					sc.nextLine();
				}
				numAlojamiento= (sc.nextInt())-1;
				sc.nextLine();
				Administrador.alojamientos.get(numAlojamiento);
				
				System.out.println("Indique la fecha de entrada(dd/mm/aa):");
				fechaEntrada = sc.nextLine();
				String regexp = "\\d{2}/\\d{2}/\\d{2}";
				//expresión regular para el foramto de la fecha
				if(!Pattern.matches(regexp, fechaEntrada))
				{
					System.out.println("Introduzca la fecha de entrada en "
							+ "el formato indicado (dd/mm/aa)");
					fechaEntrada = sc.nextLine();
				}
				while(!Pattern.matches(regexp, fechaEntrada));
				
				System.out.println("Indique el número de días:");
				while(!sc.hasNextInt()) 
				{
					System.out.println("Introduzca un numero entero.");
					sc.nextLine();
				}
				numDias=sc.nextInt();
				sc.nextLine();
				new Reserva(fechaEntrada, numDias).reservarAlojamiento(
						Administrador.alojamientos.get(numAlojamiento), 
						Usuario.usuariosHashMap.get(nombreUsuario));
				
				System.out.println("El precio total de la reserva es "+
						String.format("%.2f",Reserva.calcularPrecioTotal(
						Administrador.alojamientos.get(numAlojamiento).
						getPrecioNoche(), numDias))+" euros.");
				
			}	break;
			case 7: 
			{
				salir();
			}
				break;
			default:  
			{
				System.out.println("Opción no válida");
			}break;
		}
		}while(opcionMenu!=7);
		
		sc.close();	
	}
	
//método para filtrar alojamiento por hotel/casa rural	
	public static void busquedaAlojamientoPorTipo(String tipo) {
		
		int contador;
				
		if(tipo.equalsIgnoreCase("Casa Rural"))
		{//buscar por CasaRural
			contador=0;
			for (int i=0; i<Administrador.alojamientos.size(); i++)
			{
				if (Administrador.alojamientos.get(i) instanceof CasaRural)
				{
					mostrarAlojamientos(i);
					contador++;
				}
			}
			resultadoBusqueda(contador);
		}
		else if (tipo.equalsIgnoreCase("Hotel"))
		{//buscar por Hotel
			contador=0;
			for (int i=0; i<Administrador.alojamientos.size(); i++)
			{
				if (Administrador.alojamientos.get(i) instanceof Hotel)
				{
					mostrarAlojamientos(i);
					contador++;
				}
			}
			resultadoBusqueda(contador);
		}
	}//FIN DEL MAIN	

//método para filtrar alojamientos con precio igual o inferior al introducido
	public static void busquedaAlojamientoPorPrecioMax(double precioMax) {
		
		int contador=0;
		
		for (int i=0; i<Administrador.alojamientos.size(); i++)
		{
			if (Administrador.alojamientos.get(i).getPrecioNoche()<=precioMax)
			{
				mostrarAlojamientos(i);
				contador++;
			}
		}
		resultadoBusqueda(contador);
	}
	
//método para filtrar casas rurales por capacidad	
	public static void busquedaCasaRuralPorNumPersonas(int numPersonas) {
		int contador=0;
		for (int i=0; i<Administrador.alojamientos.size(); i++)
		{
			if (Administrador.alojamientos.get(i) instanceof CasaRural)
			{
				if(((CasaRural)Administrador.alojamientos.get(i)).
						getNumPersonas()==numPersonas)
				{
					mostrarAlojamientos(i);
					contador++;
				}
			}
		}
		resultadoBusqueda(contador);
	}

//método para filtrar hoteles por el número de estrellas 
	public static void busquedaHotelPorEstrellas(int numEstrellas) {
		int contador=0;
		for (int i=0; i<Administrador.alojamientos.size(); i++)
		{
			if (Administrador.alojamientos.get(i) instanceof Hotel)
			{
				if(((Hotel)Administrador.alojamientos.get(i)).getNumEstrellas()
						==numEstrellas)
				{
					mostrarAlojamientos(i);
					contador++;
				}
			}
		}
		resultadoBusqueda(contador);
	}
	
//método que muestra todos los alojamientos introducidos	
	public static void listarAlojamientos() {
		int contador=0;
		System.out.println("=====================");
		System.out.println("Lista de alojamientos");
		System.out.println("=====================");
		
		for (int i=0; i<Administrador.alojamientos.size(); i++)
		{
			mostrarAlojamientos(i);
			contador++;
		}
		resultadoBusqueda(contador);	
	}

//método que formatea la salida de listados de alojamientos por consola
	public static void mostrarAlojamientos(int indice) {
		if (Administrador.alojamientos.get(indice)instanceof CasaRural )
		{
			System.out.println(indice+1+". "+
				"Alojamiento: "+Administrador.alojamientos.get(indice).
					getNombre()+
				" Capacidad: " +((CasaRural) Administrador.alojamientos.
						get(indice)).getNumPersonas()+
				" Número habitaciones: "+ ((CasaRural) Administrador.
						alojamientos.get(indice)).getNumHabitaciones());
		}
		else if(Administrador.alojamientos.get(indice) instanceof Hotel)
		{
			String piscina = null;
			if(((Hotel) Administrador.alojamientos.get(indice))
				.isPiscina()==true)
				piscina="si";
			else if(((Hotel) Administrador.alojamientos.get(indice))
					.isPiscina()==false)
					piscina="no";
			System.out.println(indice+1+". "+
				"Alojamiento: "+Administrador.alojamientos.get(indice).
					getNombre()+
				" Número de estrellas: "+ ((Hotel) Administrador.alojamientos.
						get(indice)).getNumEstrellas()+
				" Piscina: "+ piscina);
		}
	}

//método que muestra el total de alojamientos listados
	public static void resultadoBusqueda(int contador)
	{
		if (contador==0)
		{
			System.out.println("No se han encontrado alojamientos");
		}
		else
		{
			System.out.println();
			System.out.println("Se muestran "+contador+ " alojamientos.");
		}
		System.out.println();
	}

//método que escribe los alojamientos en el archivo alojamientos.csv
	public static void escribirAlojamientos() 
	{
		PrintWriter printWriter = null;
		try
		{
			printWriter = new PrintWriter("alojamientos.csv");
			
				for(int i=0; i<Administrador.alojamientos.size(); i++)
				{		
					printWriter.println(Administrador.alojamientos.get(i).toString());
				}
			
			printWriter.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}	
	}

//método que escribe los usuarios en el archivo usuarios.csv
	public static void escribirUsuarios() 
	{
		PrintWriter printWriter = null;
		try
		{
			printWriter = new PrintWriter("usuarios.csv");
			
				for(String key: Usuario.usuariosHashMap.keySet())
				{		
					printWriter.println(Usuario.usuariosHashMap.
							get(key).toString());
				}
			
			printWriter.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}	
	}

//método que sale del programa
	public static void salir() 
	{
		System.out.println();
		System.out.println("Fin del programa.");
		System.exit(0);
	}
}
