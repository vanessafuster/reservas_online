package datos;

public class Hotel extends Alojamiento{
	//declaración de atributos
	private int numEstrellas;
	private boolean piscina;
	
	//método constructor
	public Hotel(String nombre, double precioNoche, int numEstrellas, boolean piscina) {
		super(nombre, precioNoche);
		this.numEstrellas = numEstrellas;
		this.piscina = piscina;
	}
	
	//métodos get y set
	public int getNumEstrellas() {
		return numEstrellas;
	}

	public void setNumEstrellas(int numEstrellas) {
		this.numEstrellas = numEstrellas;
	}

	public boolean isPiscina() {
		return piscina;
	}

	public void setPiscina(boolean piscina) {
		this.piscina = piscina;
	}

	@Override
	public String toString() {
		return nombre +";" + String.format("%.2f",precioNoche)+ ";" + 
				numEstrellas + ";" + piscina;
	}
}
