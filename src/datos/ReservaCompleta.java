package datos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ReservaCompleta {

	String nombreAlojamiento;
	String nombreUsuario;
	String fechaEntrada;
	int numDias;
	
//método constructor de la ReservaCompleto	
	public ReservaCompleta(String nombreAlojamiento, String nombreUsuario, String fechaEntrada, int numDias) {
		this.nombreAlojamiento = nombreAlojamiento;
		this.nombreUsuario = nombreUsuario;
		this.fechaEntrada = fechaEntrada;
		this.numDias = numDias;
	}
//métodos get/set	
	public String getNombreAlojamiento() {
		return nombreAlojamiento;
	}
	public void setNombreAlojamiento(String nombreAlojamiento) {
		this.nombreAlojamiento = nombreAlojamiento;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getFechaEntrada() {
		return fechaEntrada;
	}
	public void setFechaEntrada(String fechaEntrada) {
		this.fechaEntrada = fechaEntrada;
	}
	public int getNumDias() {
		return numDias;
	}
	public void setNumDias(int numDias) {
		this.numDias = numDias;
	}
//se crea el ArrayList para volcar los datos del archivo reservas.csv
	static ArrayList<ReservaCompleta> reserva4datos= new ArrayList<ReservaCompleta>();

//se vuelcan los datos del archivo reservas.csv en el ArrayList 	
	public static void volcadoArchivoReservas()
	{
		if(!(new File("reservas.csv")).exists())
		{
			System.out.println("No he encontrado el fichero " + "reservas.csv");
		}
		else
		{
			try
			{//se lee el fichero
				BufferedReader ficheroEntrada = new BufferedReader(
						new FileReader(new File("reservas.csv")));
				String linea = ficheroEntrada.readLine();
				if(linea == null)
				{
					System.out.println("El fichero está vacío.");
				}
				else
				{
					while(linea != null)
					{
						//se parte cada línea por los separadores (;), y se añade cada campo al ArrayList
						String[] datosSeparados = linea.split(";");
						
						reserva4datos.add(new ReservaCompleta(
							datosSeparados[0], 
							datosSeparados[1],
							datosSeparados[2],		
							Integer.parseInt(datosSeparados[3])));
						linea = ficheroEntrada.readLine();
					}
				}
				ficheroEntrada.close();//cierre lectura del fichero
			}catch(IOException errorDeFichero)
				{
					System.out.println("Ha habido problemas: "+ errorDeFichero.getMessage());
				}
		}
	}
//método que muestra las reservas de un usuario determinado
	public static void mostrarReservas(String nombreUsuario) 
	{
		boolean tieneReserva=false;
		for(int i=0; i<reserva4datos.size(); i++)
		{
			if(reserva4datos.get(i).getNombreUsuario().equals(nombreUsuario))
			{
				System.out.println(reserva4datos.get(i).toString());
				tieneReserva=true;
			}
		}
		if (tieneReserva==false)
		{
			System.out.println("No ha realizado reservas todavía");
		}
	}

	@Override
	public String toString() {
		return "Alojamiento:" + nombreAlojamiento + ", Fecha de Entrada:" 
				+ fechaEntrada	+ ", Número de días:" + numDias + ".";
	}
}
