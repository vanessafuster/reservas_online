package datos;

public abstract class Alojamiento {
	
	//declaración de atributos
	protected String nombre;
	protected double precioNoche;
	
	//método constructor
	public Alojamiento(String nombre, double precioNoche) {
		super();
		this.nombre = nombre;
		this.precioNoche = precioNoche;
	}
	
	//métodos get y set
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public  double getPrecioNoche() {
		return precioNoche;
	}

	public void setPrecioNoche(double precioNoche) {
		this.precioNoche = precioNoche;
	}
}
