package datos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Administrador extends Usuario{
	
//método constructor
	public Administrador(String login, String password) {
		super(login, password);
		}
	
//creación listado alojamientos
	public static ArrayList <Alojamiento> alojamientos = new ArrayList<>();
	
//método para dar de alta alojamientos
	public static void darAltaAlojamiento(Alojamiento alojamiento){
		//falta comprobar si el alojamiento ya existe
		alojamientos.add(alojamiento);
	}

	public static void darBajaAlojamiento(Alojamiento alojamiento){
		
		alojamientos.remove(alojamiento);
	}
	
	public static void volcadoAlojamientos() 
	{
		// Se comprueba si el fichero existe
		if(!(new File("alojamientos.csv")).exists())
		{
			System.out.println("No he encontrado el fichero " + "alojamientos.csv");
		}
		else
		{
			try
			{//se lee el fichero
				BufferedReader ficheroEntrada = new BufferedReader(
						new FileReader(new File("alojamientos.csv")));
				String linea = ficheroEntrada.readLine();
				if(linea == null)
				{
					System.out.println("El fichero está vacío.");
				}
				else
				{
					while(linea != null)
					{
						//se parte cada línea por los separadores (;), y se añade cada campo al ArrayList
						//distinguiendo entre Hotel o CasaRural
						String[] datosSeparados = linea.split(";");
						if (datosSeparados[3].equals("true") || 
								datosSeparados[3].equals("false"))
						{
							darAltaAlojamiento(new Hotel(
								datosSeparados[0], 
								Double.parseDouble((datosSeparados[1]).
										replaceAll(",", ".")),
								Integer.parseInt(datosSeparados[2]), 
								Boolean.parseBoolean(datosSeparados[3])));
						}
						else
						{
							darAltaAlojamiento(new CasaRural(
								datosSeparados[0], 
								Double.parseDouble((datosSeparados[1]).
										replaceAll(",", ".")),
								Integer.parseInt(datosSeparados[2]),
								Integer.parseInt(datosSeparados[3])));
						}
						linea = ficheroEntrada.readLine();
					}
				}
				ficheroEntrada.close();//cierre lectura del fichero
			}catch(IOException errorDeFichero)
				{
					System.out.println("Ha habido problemas: "+ errorDeFichero.getMessage());
				}
		}
	}
}
