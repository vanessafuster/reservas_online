package datos;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Reserva {
	
	//declaración de atributos
	private String fechaEntrada;
	private int numDias;
	
	;
	
	//método constructor
	public Reserva(String fechaEntrada, int numDias) {
		this.fechaEntrada = fechaEntrada;
		this.numDias = numDias;
		
	}
	
	
	public void reservarAlojamiento(Alojamiento alojamiento, Usuario usuario) 	
	{
		PrintWriter printWriter = null;
		try {	
			 printWriter = new PrintWriter(new BufferedWriter (new FileWriter("reservas.csv", true)));
		
			printWriter.println(alojamiento.getNombre()+";"+usuario.getLogin()+";"+ getFechaEntrada()+";"+ getNumDias());
			printWriter.close();
		}
		catch (IOException e){
			e.printStackTrace();
		}
	}

	public static double calcularPrecioTotal(double precioNoche, int numDias) {
		double precioTotal=0;
		precioTotal= precioNoche*numDias ; 
		return precioTotal;
	}
	
	//métodos get y set
	public String getFechaEntrada() {
		return fechaEntrada;
	}

	public void setFechaEntrada(String fechaEntrada) {
		this.fechaEntrada = fechaEntrada;
	}

	public int getNumDias() {
		return numDias;
	}

	public void setNumDias(int numDias) {
		this.numDias = numDias;
	}
}
