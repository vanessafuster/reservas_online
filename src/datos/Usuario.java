package datos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public abstract class Usuario {
	
	//declaración de variables
	protected String login;
	protected String password;
	
	//método constructor
	public Usuario(String login, String password) {
		super();
		this.login = login;
		this.password = password;
	}
	
	//métodos get y set
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}
	
	//creación de mapa de visitantes
			public static HashMap<String, Usuario> usuariosHashMap = 
					new HashMap<String, Usuario>();

	@Override
	public String toString() {
		return login +";"+ password;
	}
	
	public static void volcadoUsuarios() {
		// Se comprueba si el fichero existe
		if(!(new File("usuarios.csv")).exists())
		{
			System.out.println("No he encontrado el fichero " + "usuarios.csv");
		}
		else
		{
			try
			{//se lee el fichero
				BufferedReader ficheroEntrada = new BufferedReader(
						new FileReader(new File("usuarios.csv")));
				String linea = ficheroEntrada.readLine();
				if(linea == null)
				{
					System.out.println("El fichero está vacío.");
				}
				else
				{
					while(linea != null)
					{
						//se parte cada línea por los separadores (;), y se añade cada campo al hashmap de usuarios
						//distinguiendo entre visitante o administrador
						String[] datosSeparados = linea.split(";");
						if (datosSeparados[0].equals("admin"))
						{
//put nuevo administrador
							usuariosHashMap.put(datosSeparados[0],new Administrador(
								datosSeparados[0], 
								datosSeparados[1]));
						}
						else
						{
//put nuevo usuario
							usuariosHashMap.put(datosSeparados[0],new Visitante(
									datosSeparados[0], 
									datosSeparados[1]));
						}
						linea = ficheroEntrada.readLine();
					}
				}
				ficheroEntrada.close();//cierre lectura del fichero
			}catch(IOException errorDeFichero)
				{
					System.out.println("Ha habido problemas: "+ errorDeFichero.getMessage());
				}
		}
	}
}
