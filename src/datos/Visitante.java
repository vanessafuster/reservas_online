package datos;

import java.util.ArrayList;


public class Visitante extends Usuario{
	
	//declaración de atributos
	private static ArrayList <Reserva >reservas;
	
	//método constructor
	public Visitante(String login, String password) {
		super(login, password);
		reservas = new ArrayList<Reserva>();
	}
	
	//métodos get y set
	public ArrayList<Reserva> getReservas() {
		return reservas;
	}

	public static void setReservas(Reserva reserva) {
		reservas.add(reserva);
	}
}
