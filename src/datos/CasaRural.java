package datos;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class CasaRural extends Alojamiento{
	//declaración de atributos
	private int numPersonas;
	private int numHabitaciones;
	
	//método constructor
	public CasaRural(String nombre, double precioNoche, int numPersonas, int numHabitaciones) {
		super(nombre, precioNoche);
		this.numPersonas = numPersonas;
		this.numHabitaciones = numHabitaciones;
		}
	
	//métodos get y set
	public int getNumPersonas() {
		return numPersonas;
	}

	public void setNumPersonas(int numPersonas) {
		this.numPersonas = numPersonas;
	}

	public int getNumHabitaciones() {
		return numHabitaciones;
	}

	public void setNumHabitaciones(int numHabitaciones) {
		this.numHabitaciones = numHabitaciones;
	}

	@Override
	public String toString() {
		return nombre+";"+ String.format("%.2f",precioNoche) +";"+ 
				numPersonas+ ";"+ numHabitaciones;
	}
	
	public void escribirEnListado() 
	{
		
		PrintWriter printWriter = null;
		try 
		{
			printWriter = new PrintWriter(new BufferedWriter(
					new FileWriter("Alojamientos.csv",true)));
			printWriter.println();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

}
